'use client'

import React, {useEffect, useState} from "react";
import {keepPreviousData, useMutation, useQuery} from "@tanstack/react-query";
import {FetchData} from "@/component/api";
import Table from "@/component/table";
import {Column} from "primereact/column";
import {Button} from "primereact/button";
import {Dialog} from 'primereact/dialog';
import TextInput from "@/component/form/text";

const Home = () => {
    const [edit, setEdit] = useState<any>(null);
    const [addModal, setAddModal] = useState<boolean>(false);
    const [editModal, seteditModal] = useState<boolean>(false);

    const {data, error, isLoading, isFetching, refetch} = useQuery({
        queryKey: ['tableData'],
        queryFn: () => {
            return FetchData("get", "employee", "")
        },
        placeholderData: keepPreviousData
    });

    const addEmployee = useMutation({
        mutationFn: (data: any) => {
            return FetchData("post", "employee", data)
        },
    })


    const editEmployee = useMutation({
        mutationFn: (editData: any) => {
            return FetchData("put", "employee", editData)
        },
    })


    const deleteEmployee = useMutation({
        mutationFn: (editData: any) => {
            return FetchData("delete", "employee", editData)
        },
    })


    const dateTime = (data: any) => {
        return new Date(data.updateDate).toLocaleTimeString() + " " + new Date(data.updateDate).toLocaleDateString()
    };
    const dateIUpdateTime = (data: any) => {
        return new Date(data.updateDate).toLocaleTimeString() + " " + new Date(data.updateDate).toLocaleDateString()
    };

    const EditData = (data: any) => {
        setEdit(data);
    }

    const DeleteData = (data: any) => {
        deleteEmployee.mutate(data);
    }
    const addData = () => {
        setAddModal(true);
    }
    useEffect(() => {
        if (addEmployee.isSuccess || editEmployee.isSuccess || deleteEmployee.isSuccess) {
            refetch()
            HideEdit()
        }
    }, [addEmployee.isSuccess, editEmployee.isSuccess, deleteEmployee.isSuccess])


    const HideEdit = () => {
        setEdit(null);
        setAddModal(false);
        seteditModal(false);
    }

    const KirimData = () => {
        if (addModal) {
            addEmployee.mutate(edit)
        } else {
            editEmployee.mutate(edit)
        }
    }
    const actionBodyTemplate = (data: any) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" rounded outlined className="mr-2" onClick={() => EditData(data)}/>
                <Button icon="pi pi-trash" rounded outlined severity="danger" onClick={() => DeleteData(data)}/>
            </React.Fragment>
        );
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();
        // @ts-ignore
        handleChange(e)
    };

    const productDialogFooter = (
        <React.Fragment>
            <div className={"items-center"}>
                <Button label="Cancel" icon="pi pi-times" onClick={() => HideEdit()}/>
                <Button type={"submit"} label="Save" icon="pi pi-check" onClick={() => KirimData()}/>
            </div>
        </React.Fragment>
    );
    const handleChange = (e: any) => {
        let {name, value} = e.target;
        if (e.target.type === "checkbox") {
            console.log(edit[name])
            value = !edit[name]
        }

        setEdit({
            ...edit,
            [name]: value,
        });
    };

    const formBuilder = () => {
        return (
            <form onSubmit={handleSubmit}>
                <hr className="border-t-2 "/>
                <div className=" mt-8">
                    <TextInput value={edit ? edit["nip"] : ""} handleChange={handleChange}
                               name={"NIP"} keyName={"nip"}/>
                    <TextInput value={edit ? edit["name"] : ""} handleChange={handleChange} name={"Name"}
                               keyName={"name"}/>


                </div>
            </form>
        )
    }
    // @ts-ignore
    return (
        <><Table data={data} error={error} isFetching={isFetching} isLoading={isLoading} name={"Karyawan"}
                 add={addData}>
            <Column field="nip" filter header="NIP" style={{maxWidth: '4rem'}} sortable/>
            <Column field="name" filter header="Name" sortable/>

            <Column body={actionBodyTemplate} exportable={false} style={{maxWidth: '2rem'}}></Column>
        </Table>
            <Dialog visible={addModal} style={{width: '32rem'}}
                    breakpoints={{'960px': '75vw', '641px': '90vw'}}
                    header="Add Karyawan" modal className="p-fluid" onHide={HideEdit} footer={productDialogFooter}>
                {formBuilder()}
            </Dialog>

            <Dialog visible={editModal} style={{width: '32rem'}} breakpoints={{'960px': '75vw', '641px': '90vw'}}
                    header="Edit Karyawan" modal className="p-fluid" onHide={HideEdit} footer={productDialogFooter}>
                {formBuilder()}
            </Dialog>

        </>
    )
}
export default Home;
