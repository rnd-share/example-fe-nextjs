"use client";

import "./globals.css";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {ToastContainer} from "react-toastify";
import React from "react";
import {PrimeReactProvider} from "primereact/api";
import "primereact/resources/themes/mira/theme.css";
import 'primeicons/primeicons.css';
import Sidebar from "@/component/sidebar";
import Header from "@/component/sidebar/header";

export default function RootLayout({
                                       children,
                                   }: Readonly<{
    children: React.ReactNode;
}>) {
    const queryClient = new QueryClient();

    return (
        <html lang="en">
        <body>
        <QueryClientProvider client={queryClient}>
            <PrimeReactProvider>
                <div className="flex h-screen overflow-hidden">
                    <Sidebar sidebarOpen={true} setSidebarOpen={() => {
                    }}/>
                    <div className="relative flex flex-1 flex-col overflow-y-auto overflow-x-hidden">
                        <Header sidebarOpen={true} setSidebarOpen={() => {
                        }}/>
                        <div className=" w-full px-8 pt-7 ">
                            {children}
                        </div>
                    </div>
                </div>
            </PrimeReactProvider>
            <ToastContainer/>
        </QueryClientProvider>
        </body>
        </html>
    );
}
