"use client"
import React, {ReactNode} from "react";
export interface CardCommonInterface {
    children: ReactNode;
    headerNode?:boolean

}

const CardCommon = ({children,headerNode=true}:CardCommonInterface) => {

    return <div className={headerNode ? "border-2 p-4 w-full shadow" : ""}>
        {children}</div>;
};

export default CardCommon;