


 "use client"
 import React, {ReactNode} from "react";
 import {Tag} from "primereact/tag";
 export interface CardCommonInterface {
     children: ReactNode;
     headerNode?:boolean

 }

 const AlertCommon = (data: any) => {
     switch (data.status) {
         case true:
             return <Tag value={data.status} severity={"success"}>Aktif</Tag>;


         default:
             return <Tag value={data.status} severity={"danger"}>Tidak Aktif</Tag>;
     }
 };

 export default AlertCommon;