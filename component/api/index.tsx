"use client";
import axios, {AxiosInstance} from "axios";

const baseURL = "http://localhost:8080/api/";

const axiosInstance: AxiosInstance = axios.create({
    baseURL,
    headers: {
        "Access-Control-Allow-Headers":
            "Origin, X-Requested-With, Content-Type, Accept",
        "Content-Type": "application/json",
        "Access-Control-Allow-Methods": "GET, PUT, POST, DELETE, OPTIONS",
        "Access-Control-Allow-Credentials": true,
        "Cache-Control": "no-cache",
    },
});

export const FetchData = async (
    method: "post" | "put" | "get" | "patch" | "delete",
    detail: string,
    params: any
) => {
    axiosInstance.defaults.headers.common["Authorization"] =
        axios.defaults.headers.common["Authorization"];

    await new Promise((r) => setTimeout(r, 500));
    try {
        const {data} = await axiosInstance[method](detail, params);
        if (data) {
            return data.outputSchema;
        } else {
            return [];
        }
    } catch (errorReq: any) {
        const msg = errorReq.response?.data?.outputSchema;
        if (msg == "JWT is null") {
            throw new Error("Failed");
        } else {
            throw new Error("Failed");
        }
    }
};