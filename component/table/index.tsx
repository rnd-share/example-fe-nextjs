'use client'

import React, {ReactNode, useRef, useState} from "react";
import {DataTable, DataTableExpandedRows, DataTableValueArray} from 'primereact/datatable';
import {Paginator} from 'primereact/paginator';
import {ProgressSpinner} from 'primereact/progressspinner';
import {Button} from 'primereact/button';
import {Tooltip} from 'primereact/tooltip';
import {Toolbar} from "primereact/toolbar";
import CardCommon from "@/component/common/card";

export interface TableInterface {
    children: ReactNode;
    data: any,
    error: Error | null,
    isLoading: Boolean,
    isFetching: Boolean,
    name: String,
    add: () => void
    headerNode?: boolean
    rowExpansionTemplate?: any
    disabled?: boolean
    addButton?: boolean

}

const Table = ({
                   children,
                   data,
                   error,
                   isLoading,
                   isFetching,
                   name,
                   add,
                   headerNode = true,
                   rowExpansionTemplate,
                   disabled = false,
                   addButton = true
               }: TableInterface) => {
    const [page, setPage] = useState(0);
    const [rows, setRows] = useState(10);
    const dt = useRef<DataTable<any>>(null);
    const [expandedRows, setExpandedRows] = useState<DataTableExpandedRows | DataTableValueArray | undefined>(undefined);
    const expandAll = () => {
        let _expandedRows: DataTableExpandedRows = {};

        data.forEach((p: any) => (_expandedRows[`${p.id}`] = true));

        setExpandedRows(_expandedRows);
    };

    const collapseAll = () => {
        setExpandedRows(undefined);
    };

    const onPageChange = (event: any) => {
        setPage(event.page);
        setRows(event.rows);
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(data);
            const workbook = {Sheets: {data: worksheet}, SheetNames: ['data']};
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });

            saveAsExcelFile(excelBuffer);
        });
    };

    const saveAsExcelFile = (buffer: any) => {
        // @ts-ignore
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });

                module.default.saveAs(data, name + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };


    const header = (
        <>
            {headerNode &&
                <h2 className="m-0 text-2xl border-b-2 pb-8">{name}</h2>
            }
            {!disabled &&

                <div className="max-w-screen pt-4 ">
                    <Toolbar className="border-0"
                             left={ addButton ? <Button type="button" icon="pi pi-plus" severity="success" onClick={() => add()}
                                           data-pr-tooltip="XLS"> <span className="ml-2">Add</span> </Button> : ""}
                             right={headerNode ?
                                 <Button type="button" icon="pi pi-file-excel" severity="success" onClick={exportExcel}
                                         data-pr-tooltip="XLS"> <span className="ml-2">Excel</span>
                                 </Button> : ""}></Toolbar>

                </div>
            }
        </>
    );

    // @ts-ignore
    return (
        <CardCommon headerNode={headerNode}>
            {isLoading || isFetching ? (
                <div className="flex justify-center items-center h-64">
                    <ProgressSpinner/>
                </div>
            ) : error ? (
                <div className="text-red-500 text-center">Error loading data</div>
            ) : (
                <div className="card">
                    {!disabled &&
                        <Tooltip target=".export-buttons>button" position="right"/>
                    }
                    <DataTable
                        ref={dt}
                        header={header}
                        stripedRows
                        value={data}
                        paginator={false}
                        sortMode="multiple"
                        showGridlines
                        scrollable
                        {...(headerNode && {
                            expandedRows: expandedRows,
                            onRowToggle: (e) => setExpandedRows(e.data),
                            rowExpansionTemplate: rowExpansionTemplate
                        })}
                        emptyMessage="No Data found."
                        filterDisplay="menu"
                        tableStyle={{overflowX: "scroll"}}
                    >
                        {children}
                    </DataTable>
                    {headerNode &&
                        <Paginator
                            first={page * rows}
                            rows={rows}
                            totalRecords={data ? data.totalElements : 0}
                            rowsPerPageOptions={[10, 20, 50]}
                            onPageChange={onPageChange}
                            className="mt-4"
                        />}
                </div>
            )}
        </CardCommon>
    );
};

export default Table;
