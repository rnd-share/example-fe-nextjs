"use client"
import React from "react";
import {InputNumber} from "primereact/inputnumber";


export interface TextInputInterface {
    handleChange: (e: any) => void;
    value: string,
    name: string,
    keyName: string,
    disabled?: boolean,
    type?: "text" | "number" | "email" | "date" | "textarea",
    max?: number,
    header?: boolean
}

const TextInput = ({
                       value,
                       handleChange,
                       name,
                       keyName,
                       type = "text",
                       disabled = false,
                       max,
                       header = true
                   }: TextInputInterface) => {
    // @ts-ignore
    return (
        <div className="mb-4">
            {header ?
                <label className="mb-2.5 block font-medium text-black -white">
                    {name}
                </label>
                :
                <label className="mb-2.5 block font-medium text-black -white">
                </label>

            }
            <div className="relative">
                {
                    type === "number" ?
                        <InputNumber
                            placeholder={name}
                            disabled={disabled}
                            value={parseInt(value) ? parseInt(value) : 0 }
                            max={max ? max : Number.MAX_SAFE_INTEGER}
                            name={keyName}
                            min={0}
                            onChange={(e) => {
                                handleChange({"target": {"value": e.value, "name": keyName}})
                            }}
                            className="w-full text-right border rounded-lg bg-transparent h-14 text-black outline-none focus:border-primary focus-visible:shadow-none -form-strokedark -form-input -white :border-primary"
                        />
                        :
                        <input
                            type={type}
                            placeholder={name}
                            disabled={disabled}
                            value={value}
                            max={max ? max : Number.MAX_SAFE_INTEGER}
                            name={keyName}
                            min={0}
                            onChange={handleChange}
                            className="w-full rounded-lg border border-stroke bg-transparent py-4 pl-6 pr-10 text-black outline-none focus:border-primary focus-visible:shadow-none -form-strokedark -form-input -white :border-primary"
                        />
                }
            </div>

        </div>
    )
};

export default TextInput;

