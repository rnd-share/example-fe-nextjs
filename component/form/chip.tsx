"use client"
import React from "react";
import {Chips} from "primereact/chips";


export interface TextInputInterface {
    handleChange: (e: any) => void;
    value: string[],
    name: string,
    keyName: string,
    disabled?: boolean,
    max?: number,
    header?: boolean
}

const ChipInput = ({
                       value,
                       handleChange,
                       name,
                       keyName,
                       disabled = false,
                       max,
                       header = true
                   }: TextInputInterface) => {
    // @ts-ignore
    return (
        <div className="mb-4">
            {header ?
                <label className="mb-2.5 block font-medium text-black -white">
                    {name}
                </label>
                :
                <label className="mb-2.5 block font-medium text-black -white">
                </label>

            }
            <div className="relative">
                <Chips
                    placeholder={name}
                    disabled={disabled}
                    value={value}
                    max={max ? max : Number.MAX_SAFE_INTEGER}
                    name={keyName}
                    min={0}
                    separator=","
                    onChange={(e) => {
                        handleChange({"target": {"value": e.value, "name": keyName}})
                    }}
                    className="w-full text-right border rounded-lg bg-transparent h-14 text-black outline-none focus:border-primary focus-visible:shadow-none -form-strokedark -form-input -white :border-primary"
                />

            </div>

        </div>
    )
};

export default ChipInput;

