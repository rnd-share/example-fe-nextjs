"use client"
import React from "react";


export interface TextInputInterface {
    handleChange: (e: any) => void;
    value: string,
    name: string,
    keyName: string,
    disabled?: boolean,
}

const ImageInput = ({
                        value,
                        handleChange,
                        name,
                        keyName,
                        disabled = false,
                    }: TextInputInterface) => {
    return (
        <div className="mb-4">
            <label className="block mb-2 text-sm font-medium text-gray-900 ">
                {name}
            </label>


            <input
                type={"file"}
                placeholder={name}
                disabled={disabled}
                value={value}
                name={keyName}
                onChange={handleChange}
                className="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 "
            />
        </div>
    )
};

export default ImageInput;

