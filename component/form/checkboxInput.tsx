"use client"
import React from "react";


export interface TextInputInterface {
    handleChange: (e: any) => void;
    value: boolean,
    name: string,
    keyName: string,
    disabled?: boolean,
    header?: boolean,

}

const CheckboxInput = ({
                           value,
                           handleChange,
                           name,
                           keyName,
                           disabled = false,
                           header = true

                       }: TextInputInterface) => {
    return (
        <div className="mb-4">
            {header ?
                <label className="mb-2.5 block font-medium text-black -white">
                    {name}
                </label>
                :
                <label className="mb-2.5 block font-medium text-black -white">
                </label>

            }
            <div className="relative"><input
                checked={value}
                type={"checkbox"}
                placeholder={name}
                disabled={disabled}
                value="true"
                name={keyName}
                onChange={handleChange}
                className="ms-2 text-sm font-medium text-gray-900 mt-5"
            />
            </div>
        </div>
    )
};

export default CheckboxInput;

