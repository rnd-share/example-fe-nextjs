"use client"
import React from "react";
import {Dropdown} from "primereact/dropdown";
import {keepPreviousData, useQuery} from "@tanstack/react-query";
import {FetchData} from "@/component/api";


export interface TextInputInterface {
    handleChange: (e: any) => void;
    value: string,
    name: string,
    keyName: string,
    fetch: string,
    valueKey: string,
    labelKey: string,
    disabled?:boolean
    header?:boolean,
}

const SelectInput = ({value, handleChange, name, keyName, fetch,valueKey,labelKey ,disabled=false,header=true}: TextInputInterface) => {

    const {data, error, isLoading, isFetching, refetch} = useQuery({
        queryKey: [fetch],
        queryFn: () => {
            return FetchData("get", fetch, "")
        },
        placeholderData: keepPreviousData
    });
    return (
        <div className="mb-4" key={"select" + name}>
            {header &&
            <label className="mb-2.5 block font-medium text-black -white">
                {name}
            </label>
            }
            <div className="relative">
                {error ?
                    "Data Error" :
                    <>
                        <Dropdown
                            loading={isLoading}
                            value={value}
                            optionValue={valueKey}
                            optionLabel={labelKey}
                            onChange={handleChange}
                            disabled={disabled}
                            dataKey={"code"}
                            name={keyName}
                            options={data}
                            placeholder={name}
                            filter
                            className="w-full rounded-lg border border-stroke bg-transparent py-2 pl-6 pr-10 text-black outline-none focus:border-primary focus-visible:shadow-none -form-strokedark -form-input -white :border-primary"
                        />
                    </>
                }
            </div>
        </div>
    )
};

export default SelectInput;

